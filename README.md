# ninlil build script
ninlil is a simple, very generic bash build script for small projects. it requires minimal configuration, supports arbitrary directory structures & descends recursively, only compiles files that have changed since last build, and produces very pretty output in a scrollable view using `less` for the sake of readability. named after a sumerian wind goddess and pronounced [ninˈliːl] "neen-LEEL" on very little evidence. config options are at the top of the script.

## caveats
 * does not handle dependencies, so `./ninlil.sh clean` may be frequently required
 * parallel compilation support is still primitive, and will bog down your computer very easily if you have a lot of source files.
 * currently only supports one compiler at a time, tho adding ext/compiler pair support is planned
 * designed for use with clang, other compilers may require minor adaptations
 
## todo
 * logging support
 * better parallelism
 * autoclean when any header file changes

## license
ninlil is copyright 2014 alexis summer hale & released under the 3-clause BSD license.
